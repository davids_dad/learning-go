package main
import (
	"fmt"
	"net"
	"os"
	"strings"
	"log"
)

func main() {
	stderr := log.New(os.Stderr, "", 0)
	
	args := os.Args[1:]
	if len(args) == 0 {
		stderr.Println ("error: you must specify at least one arg")
		os.Exit(-1)
	}
	
	for _, hostname := range args {
		fmt.Print ( hostname, ": ")
		addrs, err :=  net.LookupHost(hostname)
		if err != nil {
			fmt.Println ( "lookup error" )
		}else{
			fmt.Println ( strings.Join( addrs, ", " ))
		}
	}
}

