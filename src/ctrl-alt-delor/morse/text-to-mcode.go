package main

import (
	"fmt"
	"bufio"
	"os"
	"io"
	"log"
)

var letters = [...]string{
	".-",
	"-...",
	"-.-.",
	"-..",
	".",
	"..-.",
	"--.",
	"....",
	"..",
	".---",
	"-.-",
	".-..",
	"--",
	"-.",
	"---",
	".--.",
	"--.-",
	".-.",
	"...",
	"-",
	"..-",
	"...-",
	".--",
	"-..-",
	"-.--",
	"--..",
}

var digits = [...]string{
	"-----",
	".----",
	"..---",
	"...--",
	"....-",
	".....",
	"-....",
	"--...",
	"---..",
	"----.",
}

var delim=""

func char_to_mcode(i rune) string {
	if (i >= 'a' && i <='z') {
		v :=(i-'a')
		result := delim+letters[v]
		delim="c"
		return result
	}else if (i >= '0' && i <='9') {
		v :=(i-'0')
		result := delim+digits[v]
		delim="c"
		return result		
	} else if (i == ' ' && delim == "c"){
		delim="w"
	} else if (i == '\n'){
		delim="l"
	}
	return ""
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	for  {
		if c, _, err := reader.ReadRune(); err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		} else {
			fmt.Printf("%s", char_to_mcode(c))	
		}
	}	
}
