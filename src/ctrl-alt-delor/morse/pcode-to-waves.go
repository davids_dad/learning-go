package main

import (
        "fmt"
        "bufio"
        "os"
        "io"
        "log"
	"time"
)

func main() {
	duration_20millisecond, _ := time.ParseDuration ("2000ms")
        reader := bufio.NewReader(os.Stdin)
        tick_time := time.Now()
        for  {
                if c, _, err := reader.ReadRune(); err != nil {
                        if err == io.EOF {
                                break
                        } else {
                                log.Fatal(err)
                        }
                } else {
			switch c {
			case '0': 
				fmt.Printf("%s", "$$$ALL,OFF\n" )
			case '1':
                                fmt.Printf("%s", "$$$ALL,ON\n")
			case '\n':
			default:
				log.Fatal("invalid input: not one or zero")
			}
                }
		tick_time = tick_time.Add(duration_20millisecond)
		//time.Sleep ( tick_time.Sub(time.Now()) )
        }
}
