package main

import (
	"time"
	"machine"
	"device/nrf"
)

const message = "hello this is a test"

var in_index = 0
func next_char() rune {
	if in_index >= len(message) { //:tricky: using length in bytes, not runes.
		in_index = 0
	}
	result := rune(message[in_index])
	in_index ++
	return result
}

var letters = [...]string{
	".-",
	"-...",
	"-.-.",
	"-..",
	".",
	"..-.",
	"--.",
	"....",
	"..",
	".---",
	"-.-",
	".-..",
	"--",
	"-.",
	"---",
	".--.",
	"--.-",
	".-.",
	"...",
	"-",
	"..-",
	"...-",
	".--",
	"-..-",
	"-.--",
	"--..",
}

var digits = [...]string{
	"-----",
	".----",
	"..---",
	"...--",
	"....-",
	".....",
	"-....",
	"--...",
	"---..",
	"----.",
}

var delim=""

func char_to_mcode(i rune) string {
	if (i >= 'a' && i <='z') {
		v :=(i-'a')
		result := delim+letters[v]
		delim="c"
		return result
	}else if (i >= '0' && i <='9') {
		v :=(i-'0')
		result := delim+digits[v]
		delim="c"
		return result
	} else if (i == ' ' && delim == "c"){
		delim="w"
	} else if (i == '\n'){
		delim="l"
	}
	return ""
}

const (
        //note long gaps are one short,
        //as there is one slot of this gap incorporated into dot&dash
        dot = "10"
        dash = "1110"
        letter_gap = "00"
        word_gap = "000000"
)

func mcode_to_pcode (c rune) string{
        switch c {
        case '.':
                return dot
        case '-':
                return dash
        case 'c':
                return letter_gap
        case 'w', 'l':
                return word_gap
        }
        return "????"
}


func pcode_to_wave (c rune) {
	machine.ClearLEDMatrix()
	if (c=='1'){
		machine.SetLEDMatrix(2,2)
		nrf.GPIO.OUTSET = (1 << 0)
	}else{
		nrf.GPIO.OUTCLR = (1 << 0)
	}
	time.Sleep(time.Millisecond * 20)
}



func main() {
	machine.InitLEDMatrix()
    for  {
        c := next_char()
		code := char_to_mcode(c)
		for _, pulse := range code {
			bits := mcode_to_pcode(pulse)
			for _, bit := range bits {
				pcode_to_wave(bit)
			}
		}
    }
}
