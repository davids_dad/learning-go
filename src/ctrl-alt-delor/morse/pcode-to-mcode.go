package main

import (
	"fmt"
	"bufio"
	"os"
	"io"
	"log"
)

const (
	//note long gaps are one short,
	//as there is one slot of this gap incorporated into dot&dash
	dot = "10"
	dash = "1110"
        letter_gap = "00"
	word_gap = "000000"	
)

var high = false
var span = 7

func pcode_to_mcode (c rune) string{
	result := ""
	if c == '0' && high {
		switch span {
		case 1:
			result= "."
		case 3:
			result= "-"
		default:
			result= "?"
		}
		span=0
	} else if c == '1' && !high {
		//fmt.Printf("%c", c)
		switch span {
		case 1:
			result= ""
		case 3:
			result= "c"
		case 7:
			result= "w"
		default:
			result= "!"
		}
		span=0
	}
	high = (c == '1')
	span++
	return result;
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	for  {
		if c, _, err := reader.ReadRune(); err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		} else {
			fmt.Printf("%s", pcode_to_mcode(c))	
		}
	}	
}
