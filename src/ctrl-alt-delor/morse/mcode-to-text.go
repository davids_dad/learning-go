package main

import (
	"fmt"
	"bufio"
	"os"
	"io"
	"log"
)

var letters = [...]string{
	".-",
	"-...",
	"-.-.",
	"-..",
	".",
	"..-.",
	"--.",
	"....",
	"..",
	".---",
	"-.-",
	".-..",
	"--",
	"-.",
	"---",
	".--.",
	"--.-",
	".-.",
	"...",
	"-",
	"..-",
	"...-",
	".--",
	"-..-",
	"-.--",
	"--..",
}

var digits = [...]string{
	"-----",
	".----",
	"..---",
	"...--",
	"....-",
	".....",
	"-....",
	"--...",
	"---..",
	"----.",
}

var buffer [10]rune
var buf = buffer[:0]

func reset_buf(){
	buf = buffer[:0]	
}

func find_ascii_code(table []string, base rune, morse []rune) string {
	for i:=0; i < len(table); i++ {
		if string(morse) == table[i] {
			return string(rune(i+int(base)))
		}
	}
	return ""
}

func ascii_code(morse []rune) string {
	if len(buf) == 0 {
		return ""
	}
	result := ""
	result = find_ascii_code(letters[:], 'a',  morse)
	if result != "" {
		return result
	}
	result = find_ascii_code(digits[:], '0', morse)
	if result != "" {
		return result
	}
	return "?"
}

func add_to_buf( c rune ) {
	if len(buf)>=cap(buf) {
		log.Fatal("code to long")
	}
	buf = append(buf, c)
}

func mcode_to_ascii(c rune) string {
	delim := ""
	if c == '.' || c == '-' {
		add_to_buf(c)
		return ""
	}else if c == 'c' {
		delim=""
	}else if c == 'w' {
		delim=" "
	}else if c == 'l' {
		delim="\n"
	}else if c == 'e' {
		delim=""
	}

	result := ascii_code(buf)+delim
	reset_buf()
	return result
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	for  {
		if c, _, err := reader.ReadRune(); err != nil {
			if err == io.EOF {
				fmt.Printf("%s", mcode_to_ascii('e'))
				break
			} else {
				log.Fatal(err)
			}
		} else {
			fmt.Printf("%s", mcode_to_ascii(c))	
		}
	}	
}
