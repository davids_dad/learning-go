package main

import (
	"fmt"
	"bufio"
	"os"
	"io"
	"log"
)

const (
	//note long gaps are one short,
	//as there is one slot of this gap incorporated into dot&dash
	dot = "10"
	dash = "1110"
        letter_gap = "00"
	word_gap = "000000"	
)

func mcode_to_pcode (c rune) string{
	switch c {
	case '.':
		return dot
	case '-':
		return dash
	case 'c':
		return letter_gap
	case 'w', 'l':
		return word_gap
	}
	return "????"
}



func main() {
	reader := bufio.NewReader(os.Stdin)
	for  {
		if c, _, err := reader.ReadRune(); err != nil {
			if err == io.EOF {
				break
			} else {
				log.Fatal(err)
			}
		} else {
			fmt.Printf("%s", mcode_to_pcode(c))	
		}
	}	
}
