package main

import (
	"fmt"
	"os"
	"encoding/json"
	"github.com/spf13/cobra"
	"github.com/cbroglie/mustache"
)

var rootCmd = &cobra.Command{
	Use: "my-mustache",
	Example: `  $ my-mustache`,
	Args: cobra.RangeArgs(0,2),
	Run: func(cmd *cobra.Command, args []string) {
		err := run(cmd, args)
		if err != nil {
			fmt.Printf("Error: %s\n", err.Error())
			os.Exit(1)
		}
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func run(cmd *cobra.Command, args []string) error {
 
	var templatePath string = "my.mustache"
	var data = map[string] interface{} {
		"hello" : "world",
		"hi" : "there",
		"sub": map[string] string {
			"a": "123",
			"b": "zyz",
		},
	}
			
	
	fmt.Printf("%+v\n", data )
	fmt.Printf("----------------------------------------------\n")
	dmsg, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Print(string(dmsg) )

	fmt.Printf("\n----------------------------------------------\n")

	output, err := mustache.RenderFile(templatePath, data)
	if err != nil {
		return err
	}

	fmt.Print(output)
	return nil
}


